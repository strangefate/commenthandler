<?php

namespace StrangeFate\CommentHandler\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use StrangeFate\CommentHandler\Comment;

class CommentController extends Controller
{
	public function __construct() {
        $this->middleware(["auth"]);
    }

    public function reply(Request $request, Comment $comment) {
        if( config('comments.features.reply') ){
            $message = $request->validate(['message' => 'required']);
            $message['user_id'] = auth()->id();

            $comment->comments()->create($message);
        }

        return redirect()->back();
    }

    public function like(Request $request, Comment $comment) {
        if( config('comments.features.like') )
            $comment->like( auth()->id() );

        return redirect()->back();
    }

    public function report(Request $request, Comment $comment) {
        if(config('comments.features.report') && !$comment->isReportedBy( auth()->id() ) )
            $comment->reports()->attach( auth()->id() );

        if( config('comments.report.auto_hide') && $comment->reports()->count() >= config('comments.report.report_limit') )
            $comment->hide();

        return redirect()->back();
    }
}
