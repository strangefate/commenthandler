<?php

namespace StrangeFate\CommentHandler\Interfaces;

use Illuminate\Database\Eloquent\ModelNotFoundException;

trait HasLikes
{
	public function likes() {
		try {
			return $this->morphToMany(\App\Models\User::class, 'likable', 'comment_likes');
		} catch (ModelNotFoundException $e) {
			return $this->morphToMany(\App\User::class, 'likable', 'comment_likes');
		}
	}

	public function likeCount() {
		return $this->likes()->count();
	}

	public function like($id) {
		if($this->likes()->where( 'user_id', '=', $id )->count() == 0 )
            $this->likes()->attach( $id );
	}

	public function likeWithResponse($id) {
		$return = [];
		if($this->likes()->where( 'user_id', '=', $id )->count() == 0 )
            $this->likes()->attach( $id );
        else
        	$return['message'] = 'You have already liked this item.';

        $return['likes'] = $this->likeCount();

        return $return;
	}
}