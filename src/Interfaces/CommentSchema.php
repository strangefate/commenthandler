<?php

namespace StrangeFate\CommentHandler\Interfaces;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentSchema extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "@context" => "http://schema.org",
            "@type" => "Comment",
            "author" => [
                "@type" => "Person",
                "name" => $this->user->name,
            ],
            "dateCreated" => $this->created_at->toIso8601String(),
            "text" => $this->comment,
        ];
    }
}
