<?php

namespace StrangeFate\CommentHandler\Interfaces;

class CommentHelperFunctions {

	public static function profanity_filter($text) {

		$profanity =  config('comments.profanity.curses');  //filtered words
		$alias =  config('comments.profanity.alias'); //rules to remove common work arounds for curse words.

		foreach($profanity as $curse) {
			$str = implode("#", str_split($curse['curse'])); //# identifies characters to circumvent
			$str = str_replace(array_keys($alias), $alias, $str);
			$text = preg_replace('/([\s\W_]+)' . $str . '+([\s\W_]+)/i', "$1" . $curse['filter'] . "$2", $text);
			$text = preg_replace('/^' . $str . '+([\s\W_]+)/i', $curse['filter'] . "$1", $text);
			$text = preg_replace('/([\s\W_]+)' . $str . '$/i', "$1" . $curse['filter'], $text);
			$text = preg_replace('/^' . $str . '$/i', $curse['filter'], $text);
		}
			
		return $text != "" ? $text : null;
	}

}