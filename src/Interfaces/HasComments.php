<?php

namespace StrangeFate\CommentHandler\Interfaces;

use StrangeFate\CommentHandler\Comment;

trait HasComments
{
	public function comments() {
		return $this->morphMany(Comment::class, 'commentable');
	}
}