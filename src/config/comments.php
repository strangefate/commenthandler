<?php

return [
	'features' => [
		'reply' => true,
		'like' => true,
		'report' => true,
	],

	'report' => [
		'auto_hide' => true,
		'report_limit' => 10
	],

	'profanity' => [

		//curse words and the replacements
		'curses' => [
			['curse' => 'ass', 'filter' => 'butt'],
			['curse' => 'damn', 'filter' => 'darn'],
		],

		//possible letter aliases to search for in strings, like @$$ = ass
		'alias' => [
			'a' => '[a|@]',
			'i' => '[i|!]',
			'o' => '[o|0]',
			's' => '[s|\$]',
			'#' => '+[\s\W_]?' //spaced curse words. DO NOT REMOVE!
		]
	]
];