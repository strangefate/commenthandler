<?php

namespace StrangeFate\CommentHandler;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Like extends Model
{
	public function source() {
		return $this->morphTo();
	}

	public function user() {
		try {
			return $this->belongsTo(\App\Models\User::class);
		} catch (ModelNotFoundException $e) {
			return $this->belongsTo(\App\User::class);
		}
	}

	protected $guarded = [];
	protected $table = 'comment_likes';
	public $timestamps = false;
}