<?php

namespace StrangeFate\CommentHandler;

use Illuminate\Database\Eloquent\Model;
use StrangeFate\CommentHandler\Comment;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Report extends Model
{
	public function comment() {
		return $this->belongsTo(Comment::class);
	}

	public function user() {
		try {
			return $this->belongsTo(\App\Models\User::class);
		} catch (ModelNotFoundException $e) {
			return $this->belongsTo(\App\User::class);
		}
	}

	protected $table = 'comment_reports';
	public $timestamps = false;
}