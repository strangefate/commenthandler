<?php 
namespace StrangeFate\CommentHandler;

use Illuminate\Support\ServiceProvider;

class CommentHandlerServiceProvider extends ServiceProvider
{
	public function boot() {

		$this->loadRoutesFrom( __DIR__.'/routes/web.php' );
		$this->mergeConfigFrom(__DIR__.'/config/comments.php', 'comments' );
		$this->loadMigrationsFrom(__DIR__.'/migrations');
		$this->loadViewsFrom(__DIR__.'/views', 'comments');

		$this->publishes([
			__DIR__.'/config/comments.php' => config_path('comments.php'),
		], 'config');

		$this->publishes([
			__DIR__.'/views' => resource_path('views/vendor/comments'),
			__DIR__.'/sass' => resource_path('sass')
		], 'views');
	}

	public function register() {
		
	}
}