<?php

Route::group(['namespace' => 'StrangeFate\CommentHandler\Controllers', 'middleware' => ['web']], function() {
	Route::post('/comments/{comment}/reply', 'CommentController@reply');
	Route::post('/comments/{comment}/like', 'CommentController@like');
	Route::post('/comments/{comment}/report', 'CommentController@report');
});