@foreach($comments as $comment)
<div class="cell comment-wrapper">
	<div class="grid-x grid-padding-x">
		<div class="cell shrink comment-member-panel">
			<span class="commenter-name">{{ $comment->user->name }}</span>
			<br />
			<b>Member Since</b> <br />
			{{ $comment->user->created_at->toFormattedDateString() }} <br />
			<b>Comments:</b> {{ $comment->user->comments()->count() }}
		</div>
		<div class="cell auto comment-message-panel">
			<h6>Replied {{ $comment->created_at->toDayDateTimeString() }}</h6>
			<p>
				{{ \StrangeFate\CommentHandler\Interfaces\CommentHelperFunctions::profanity_filter($comment->message) }}
			</p>
			<div class="comment-footer">
				<div class="tiny button-group">
					@if( config('comments.features.reply') )
						<a class="button" data-toggle="comment-reply-{{ $comment->id }}">
							Reply
						</a>
					@endif

					@if( config('comments.features.like') )
						<form method="POST" action="{{ url("comments/$comment->id/like") }}">
							@csrf
							<input class="button" style="margin-right: 1px;" type="submit" value="Like: {{ $comment->likeCount() }}">
						</form>
					@endif

					@if( config('comments.features.report') )
						<form method="POST" action="{{ url("comments/$comment->id/report") }}">
							@csrf
							<input class="button" type="submit" value="Report: {{ $comment->reports()->count() }}">
						</form>
					@endif
				</div>
				
				@if( config('comments.features.reply') )
					<div id="comment-reply-{{ $comment->id }}" class="callout comment-reply-container" data-toggler data-animate="hinge-in-from-top hinge-out-from-top" style="display: none;">
						@include('comments::create', ['url' => "/comments/$comment->id/reply"])
					</div>
				@endif
			</div>

			@if($comment->has('comments'))
				@include('comments::comment_list', ['comments' => $comment->comments()->show()->get()])
			@endif
		</div>
	</div>
</div>
@endforeach