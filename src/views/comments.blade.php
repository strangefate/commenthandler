<div class="grid-container">
	<div class="grid-x grid-margin-x comment-group-wrapper">
		
		@include('comments::comment_list', ['comments' => $comments])

		@auth
			@include('comments::create', ['url' => $url])
		@else
			<p>You must be logged in to add a comment.</p>
		@endauth

	</div>
</div>