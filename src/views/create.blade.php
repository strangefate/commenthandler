<div class="cell">
	
	<form method="POST" action="{{ $url }}">
		@csrf
		<div class="grid-x comment-form-wrapper">
			<div class="cell comment-textboxt-container">
				<label for="message">Enter your comment:
					<textarea id="message" name="message" rows="3"
						placeholder="Leave a message"></textarea>
				</label>
			</div>

			<div class="cell">
				<input class="button expanded small" type="submit" value="Submit">
			</div>
		</div>
	</form>
</div>