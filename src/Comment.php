<?php

namespace StrangeFate\CommentHandler;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use StrangeFate\CommentHandler\Interfaces\HasLikes;
use StrangeFate\CommentHandler\Interfaces\HasComments;

class Comment extends Model
{
    use HasLikes, HasComments;

	protected $fillable = ['user_id', 'message'];
    protected $with = ['user', 'likes', 'reports', 'comments'];

    public function user() {
		try {
			return $this->belongsTo(\App\Models\User::class);
		} catch (ModelNotFoundException $e) {
			return $this->belongsTo(\App\User::class);
		}
	}

    public function toogle() {
        $this->isActive != $this->isActive;
        return $this;
    }

    public function reports() {
        try {
            return $this->belongsToMany(\App\Models\User::class, 'comment_reports');
        } catch (ModelNotFoundException $e) {
            return $this->belongsToMany(\App\User::class, 'comment_reports');
        }
    }
    
    public function scopeActive($query) {
		return $query->where('isActive',1);
	}

    public function scopeReported($query, $count = 1) {
        return $query->has('reports', $count);
    }

    public function scopeShow($query) {
        return $query->active()->with('user.comments')->with('comments.comments')->with('comments.likes')->with('comments.reports');
    }

    public function hide() {
        $this->isActive = false;
        $this->save();
    }

    public function isReportedBy($user_id) {
        $count = $this->reports()->where('user_id', $user_id)->count();

        return $count > 0;
    }
}