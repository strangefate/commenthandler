This comment handler is a basic package for adding quick comments and likes to your project. It includes a profanity filter and users can report inappropriate posts. Posts that reach a configured report thresh hold will automatically be suspended from the site.

### Package Installation

Require package
```php
composer require strangefate/commenthandler
```

Run migration to add the required table for comments, likes and reports to the database
```php
php artisan migrate
```

Add the relationship for Users and their comments in **app\User.php**

```php
use StrangeFate\CommentHandler\Comment;

...
public function comments() {
    return $this->hasMany(Comment::class);
}
```

### Configuration
By default, all options are turned on for comments (Like, Report, and Reply to comments). Publish the config file to configure the platforms you want.
```php
php artisan vendor:publish --provider=StrangeFate\CommentHandler\CommentHandlerServiceProvider --tag=config
```

This will publish the comment config file to your config directory:
```cmd
app\config\comments.php
```

Once you have your configuration set, you will need to make your Models to have comments or likes. Simply go into your model and add the interfaces HasComment or HasLike to attach those features.

```php
<?php

namespace App;

use StrangeFate\CommentHandler\Interfaces\HasLikes;
use StrangeFate\CommentHandler\Interfaces\HasComments;

class MyNewClass extends Model
{
    use HasComments, HasLikes;
}
```

This will add all the necessary relationship for your model to accept comments and/or likes. Comments will automatically have the routes for likes built in, but you will need to create the routes and logic to attach for your models to accept them.

```php
Route::post('mynewclasses/{mynewclass}/comment', function(Request $request, MyNewClass $mynewclass) {
    $comment = $request->validate(['message' => 'required']);
    $comment['user_id'] = auth()->id();

    $mynewclass->comments()->create($comment);

    return redirect("/mynewclass/$mynewclass->id");
});

Route::post('mynewclasses/{mynewclass}/like', function(Request $request, MyNewClass $mynewclass) {
    $mynewclass->like( auth()->id() ); //The like() function will detect if the user has already liked the item

    return redirect("/mynewclass/$mynewclass->id");
});
```
Comments automatically accept likes and appropriate routing logic. If you do not wish to have a like function on your comments, you can turn it off in the config\comments.php file.

### View templates

The comment handler comes with some pre-made templates for you to include in your project.
```blade
@include('comment::comments', ['comments' => $module->comments->show()->get(), 'url' => 'post/route/for/submitting/comments']) {-- A general output for comments --}
@include('comment::comment_list', ['comments' => $comments] ) {-- for defining the format in which post are laid out. --}
@include('comment::create', ['url' => 'post/route/for/submitting/comments'])  {-- The form for submitting a comment --}
@include('comment::schema', ['comments' => $comments]) {-- The schema.org json for dumping comments into your page for scrapping --}
```

You can publish the views to your views\vendor folder to customize the views for the site
```php
php artisan vendor:publish --provider=StrangeFate\CommentHandler\CommentHandlerServiceProvider --tag=views
```

Publishing the views will also dump a sass file with all the predefined classes used in the template for you to style comments for your site. Simply include it in your **app.scss** and add your css.
```scss
@import "comments";
```

### Reporting Post
This module contains some logic to allow users to moderate the board by reporting inappropriate comments. You can disable this feature all together by setting the report option to false in the **config\comments.php** file after publishing it.

You can set the limits for reports to automatically hide a post by configuring the report array in the same config file.

```php
    'report' => [
        'auto_hide' => true, //automatically hide a comment after it his been reported so many times
        'report_limit' => 10 //the number of times a post can be reported before it is automatically hidden
    ],
```

You can check the reported comments by using a query scope, and the list of users that have reported a comment through it's relationship methods.
```php
    $comments = Comment::reported()->get() //gets a list of all comments that have been reported.
    $comments->first()->reports()->pluck('name') //gets a list of users that have reported the comment.
```

### Profanity filter
The Comment Handler comes with a profanity filter for obscuring vile language that might come through the comments. You can expand on this list by adding words to the curses array in the **config\comments.php**

```php
'curses' => [
    ['curse' => 'badword', 'filter' => 'replacement'],
],
```
The profanity filter will also pick up attempts to bypass the filter by replacing characters with symbols. You can add to this by expanding on the alias array in the **config\comment.php** to add regex expressions to catch additional characters.

```php
'alias' => [
    'a' => '[a|@]',
    'i' => '[i|!]',
    'o' => '[o|0]',
    's' => '[s|\$]',
    '#' => '+[\s\W_]?' //spaced curse words. DO NOT REMOVE!
]
```

The above array with catch the strings 'b@dword', 'badw0rd', 'b@dw0rd', 'b a d w o r d' and more.

You can use the profanity filter anywhere in your project you wish to filter potential obscenities.
```blade
<div>
    {{ \StrangeFate\CommentHandler\Interfaces\CommentHelperFunctions::profanity_filter( $filtertext ) }}
</div>
```